# README #

This library is openly consumable as a C# class library

### What is this repository for? ###

* This codebase is designed to provided a library for building local databases by utilizing SQL Server Management Objects (SMO) to create SQL objects for building a scoped database.
* The library is intended to address environmental issues within teams by allowing a team to work locally on a database amongst multiple users by creating a Database Build structure that can be shared
* amongst users. 
* This project is still a work in progress.
* * The library outputs SQL files in a Database Build Folder. The contents of this folder will be executed against a local DB. 
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

*Consume as a normal class library. All classes and methods are static to avoid creating mulitple instances of internal scripter objects which causes collision issues when trying to build out database.

### Future ambitions of the project ###

* Build out local databases from a given database context
	- Currently this includeds:
		- Functions
		- Stored Procedures
		- Custom DataTypes
		- Indexes
		- Schemas
		- Tables (with or without data)
		

		

### Who do I talk to? ###

* Cory Hall 
* Email: cory_hall88@yahoo.com
