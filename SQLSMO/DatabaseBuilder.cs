﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using Microsoft.SqlServer.Management.Smo;
using System.Data.SqlClient;

namespace SQLSMO
{
    class DatabaseBuilder
    {
        static readonly Server Srv = new Server(@"(localdb)\MSSQLLocalDB");
        private static String DatabaseName { get; set; }
        static readonly Database Db = Srv.Databases[DatabaseName];

        private void BuildLocalDatabase()
        {
           DatabaseEnumerator.CreateDatabase(Srv,DatabaseName);
        }

        private List<SqlSmoObject> GetListOfStoredProcs()
        {
            return DatabaseEnumerator.ScriptStoredProcedures(Srv, Db,false);
        }

        private void GetObjectDependencies()
        {
            DatabaseEnumerator.GetStoredProcedureDependencies(Srv,Db , GetListOfStoredProcs()[0].ToString());
        }

        
    }
}
