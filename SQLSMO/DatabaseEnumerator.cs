﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using Microsoft.SqlServer.Management.Common;

namespace SQLSMO
{
    public class DatabaseEnumerator
    {
        public static void Main(string[] args)
        {


            string dbName = "Adventureworks2012";
            Server server = new Server(@"(localdb)\MSSQLLocalDB");

            Database db = server.Databases[dbName];
            string NewDatabaseName = "TestDatabase";

            CreateDatabase(server, NewDatabaseName);
            Console.WriteLine("Loading Query");
            SqlConnection conn =
                new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TestDatabase"].ConnectionString);

            Console.WriteLine("Executing");

            //foreach (var VARIABLE in GetStoredProcedureDependencies(server, db, "uspGetEmployeeManagers"))
            //{
            //    Console.WriteLine(VARIABLE);
            //}


            //FileInfo SQLFile = new FileInfo(@"C:\users\block\desktop\sqlscripts\TEST.sql");

            //Console.WriteLine("Scripting out Database objects");
            //ScriptDataBaseObjectsToDataBaseBuildFolder(server, db);

            //Console.WriteLine("Writing objects to new database!");

            //LoadDBFromListofSQLFiles(conn);

            //if (tb.IsSchemaOwned == false)
            //{
            //    Console.WriteLine("-- Scripting for Table " + tb.Name);

            //   // CreateSqlFiles(scrp, tb);

            //}




            //ScriptATable(server,db,"Employee", "HumanResources");
            //ScriptOutAllTables(server,db,true);
            //ScriptOutAllTableProperties(server, db);
            //LoadDatabaseFromSQLFile(SQLFile, conn);
            //Console.WriteLine("Finished Executing");

            //db = server.Databases["TestDatabase"];
            //Console.WriteLine(db.CreateDate);
            //Gets list of stored procedures in database


            string objName = "uspGetBillOfMaterials";

            //string tableName = "Employee";
            //string Schema = "HumanResources";

            //ScriptATable(server, db, tableName, Schema);

            //Gets a list of dependencies for a given Stored Procedure
            Dictionary<string, string> proclist = GetStoredProcedureDependencies(server, db, objName);

            foreach (var obj in proclist)
            {
                ScriptATable(server, db, obj.Key, obj.Value);
            }

            //Scripts out all tables in Database
            // ScriptOutAllTables(server, db, false);


            Console.Write("--Done--");
            Console.ReadKey();

        }

        private static void ScriptDataBaseObjectsToDataBaseBuildFolder(Server server, Database db)
        {
            GetSchemaLayout(db);
            ScriptOutAllTables(server, db, false);
            ScriptUserDefinedFunction(server, db);
            ScriptUserDefinedDataTypes(server, db);
            ScriptStoredProcedures(server, db, true);
            ScriptOutAllTableProperties(server, db);
        }

        private static void ScriptUserDefinedFunction(Server server, Database db)
        {
            Scripter scrp = new Scripter(server);
            scrp.Options.ScriptBatchTerminator = true;
            Directory.CreateDirectory(@".\Database Build\6Functions\");
                foreach (UserDefinedFunction fn in db.UserDefinedFunctions)
                {
                    if (!fn.IsSystemObject)
                    {
                        using (StreamWriter sw =
                            new StreamWriter(@".\Database Build\6Functions\USERDEFINEDFUNCTIONS." + fn.Name + ".sql"))
                        {

                            Console.WriteLine(fn.Name);
                            foreach (string s in fn.Script())
                            {
                                if (s.Contains("SET ANSI_NULLS ON") || s.Contains("SET QUOTED_IDENTIFIER ON"))
                                {
                                }
                                else
                                {
                                    Console.WriteLine(s);
                                    sw.WriteLine(s);
                                }
                                
                            }

                        }
                    }

                }
            
        }

        private static void ScriptUserDefinedDataTypes(Server server, Database db)
        {
            Scripter scrp = new Scripter(server);
            Directory.CreateDirectory(@".\Database Build\2Datatypes\");
                foreach (UserDefinedDataType tb in db.UserDefinedDataTypes)
                {
                    using (StreamWriter sw = new StreamWriter(@".\Database Build\2Datatypes\USERDEFINEDDATATYPES."+ tb.Name +".sql"))
                    {
                        Console.WriteLine(tb.Name);
                        foreach (string s in tb.Script())
                        {
                            Console.WriteLine(s);
                            sw.WriteLine(s);
                        }
                    }
                }
            
        }

        private static void GetSchemaLayout(Database db)
        {
            
                Directory.CreateDirectory(@".\Database Build\1Schema\");
            

                for (int i = 0; i < db.Schemas.Count; i++)
                {

                if (db.Schemas[i].IsSystemObject == false)
                    {
                        using (StreamWriter sw =
                            new StreamWriter(@".\Database Build\1Schema\"+ db.Schemas[i].Name+ ".sql"))
                        {
                            Console.WriteLine("CREATE SCHEMA " + db.Schemas[i].Name);
                            Console.WriteLine("GO;");


                            sw.WriteLine("CREATE SCHEMA " + db.Schemas[i].Name + Environment.NewLine);
                           
                        }
                    }
                }
            
        }

        private static void LoadDBFromListofSQLFiles(SqlConnection conn)
        {
            string[] fileEntries = GatherListOfSQLFiles();

            foreach (string fileName in fileEntries)
            {

                try
                {
                    LoadDatabaseFromSQLFile(new FileInfo(fileName), conn);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }


        }

        private static string[] GatherListOfSQLFiles()
        {
            return Directory.GetFiles(@".\Database Build\", "*.sql", SearchOption.AllDirectories);
        }

        /// <summary>
        /// Loads given SQL files and executes them against the database.
        /// </summary>
        /// <param name="Script"></param>
        /// <param name="conn"></param>
        public static void LoadDatabaseFromSQLFile(FileInfo SQLFile, SqlConnection conn)
        {
            String Script = SQLFile.OpenText().ReadToEnd();
            SqlCommand cmd = new SqlCommand(Script, conn);
            try
            {
                cmd.Connection.Open();
                if (cmd.Connection.State != ConnectionState.Closed)
                {
                
                    cmd.ExecuteScalar();

                    Console.WriteLine(SQLFile.Name + " Has been created!");
                    cmd.Connection.Close();
                }
                else
                {
                    cmd.Connection.Open();

                    cmd.ExecuteScalar();

                    Console.WriteLine(SQLFile.Name + " Has been created!");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                cmd.Connection.Close();
            }

            //using (SqlCommand cmd = new SqlCommand(Script, conn))
            //{
            //    if (cmd.Connection.State != ConnectionState.Open)
            //    {
            //        cmd.Connection.Open();
            //        cmd.ExecuteNonQuery();
            //        //cmd.Connection.Close();
            //    }
            //    else
            //    {
            //        cmd.Connection.e
            //    }


            //}

        }

        /// <summary>
        /// Creates a new local database
        /// </summary>
        /// <param name="server"></param>
        /// <param name="dbName"></param>
        public static void CreateDatabase(Server server, String dbName)
        {
            if (server.Databases[dbName] == null)
            {
                Database database = new Database(server, dbName);
                database.Create();
            }
            
        }

        /// <summary>
        /// Gets a list of stored procedure dependencies for a given stored procedure name
        /// </summary>
        /// <param name="server">Name of the server the stored procedure lives on</param>
        /// <param name="db">Name of the database the stored procedure lives on</param>
        /// <param name="objName">Name of the stored procedure to get the dependencies for</param>
        public static Dictionary<string, string> GetStoredProcedureDependencies(Server server, Database db, string objName)
        {
            UrnCollection col = new UrnCollection();
            Dictionary<string, string> ProcDependencies = new Dictionary<string, string>();
            foreach (StoredProcedure storedProc in db.StoredProcedures)
            {
                if (storedProc.Name == objName)
                    col.Add(storedProc.Urn);
            }

            DependencyWalker dependencyWalker = new DependencyWalker(server);
            DependencyTree dependencyTree = dependencyWalker.DiscoverDependencies(new Urn[] { db.StoredProcedures[objName, "dbo"].Urn }, DependencyType.Parents);
            DependencyCollection dependencyCollection = dependencyWalker.WalkDependencies(dependencyTree);
            Console.WriteLine("Dependency objects for: " + objName);
            //Console.WriteLine(dependencyTree.NumberOfSiblings);
            foreach (DependencyCollectionNode dcn in dependencyCollection)
            {
                if (dcn.Urn.Type != "UnresolvedEntity")
                {
                    ProcDependencies.Add(dcn.Urn.XPathExpression.GetAttribute("Name", dcn.Urn.Type), dcn.Urn.XPathExpression.GetAttribute("Schema", dcn.Urn.Type));
                    Console.WriteLine("Type: " + dcn.Urn.Type + " Schema: " + dcn.Urn.XPathExpression.GetAttribute("Schema", dcn.Urn.Type) + " Object Name: " + dcn.Urn.XPathExpression.GetAttribute("Name", dcn.Urn.Type));
                }
                
                //Console.WriteLine(dcn.Urn.Type + " ----- " + dcn.Urn.XPathExpression.GetAttribute("Name",dcn.Urn.Type));
            }

            return ProcDependencies;
        }


        /// <summary>
        /// Gets a list of Stored Procedures for a given server and database
        /// </summary>
        /// <param name="server">Name of the server the stored procedure lives on</param>
        /// <param name="db">Name of the database the stored procedure lives on</param>
        /// <returns>A list of SQLSMOObjects</returns>
        public static List<SqlSmoObject> ScriptStoredProcedures(Server server, Database db, bool ScriptProc)
        {
            List<SqlSmoObject> StoredProcList = new List<SqlSmoObject>();
            DataTable datatable = db.EnumObjects(DatabaseObjectTypes.StoredProcedure);

            Directory.CreateDirectory(@".\Database Build\5Procedures\");
            foreach (DataRow row in datatable.Rows)
            {
                string sSchema = (string)row["Schema"];
                if (sSchema == "sys" || sSchema == "INFORMATION_SCHEMA")
                    continue;
                StoredProcedure sp = (StoredProcedure)server.GetSmoObject(
                    new Urn((string)row["Urn"]));
                if (!sp.IsSystemObject)
                    StoredProcList.Add(sp);

                if (ScriptProc && !sp.IsSystemObject)
                {
                    using (StreamWriter sw = new StreamWriter(@".\Database build\5Procedures\" + sp.Name + ".sql"))
                    {
                        foreach (string s in sp.Script())
                        {
                            if (s.Contains("SET ANSI_NULLS ON") || s.Contains("SET QUOTED_IDENTIFIER ON")) { }
                            else
                            {
                                sw.WriteLine(s.ToString());
                            }
                        }
                    }
                    
                }
            }

            foreach (SqlSmoObject item in StoredProcList.ToArray())
            {
                Console.WriteLine("Stored Procedure: " + item);
                //scripter.Options.FileName = @"C:\Users\block\desktop\" + item + ".sql";
                //Console.WriteLine(scripter.Options.FileName);

            }
            return StoredProcList;
           
        }

        /// <summary>
        /// Scripts out every table in a given database
        /// </summary>
        /// <param name="server">Name of the server the stored procedure lives on</param>
        /// <param name="db">Name of the database the stored procedure lives on</param>
        /// <param name="scriptData">Determines rather to script out Schema only or data</param>
        private static void ScriptOutAllTables(Server server, Database db, Boolean scriptData)
        {
            Scripter scrp = new Scripter(server);

            scrp.Options.ScriptData = scriptData;
            scrp.Options.ScriptDrops = false;
            scrp.Options.WithDependencies = false;
            scrp.Options.Indexes = false;
            scrp.Options.DriAllConstraints = false;
            scrp.Options.Permissions = false;

            foreach (Table tb in db.Tables)
            {
                if (tb.IsSystemObject == false)
                {
                    Console.WriteLine("-- Scripting for Table " + tb.Name);
                   
                    CreateSqlFiles(scrp, tb);

                }
            }
        }

        private static void ScriptOutAllTableProperties(Server server, Database db)
        {
            Scripter scrp = new Scripter(server);
            Directory.CreateDirectory(@".\Database Build\4Indexes\");
            
            scrp.Options.Indexes = true;
            scrp.Options.DriAllConstraints = true;
            scrp.Options.Permissions = true;
            
                foreach (Table tb in db.Tables)
                {
                    foreach (Index index in tb.Indexes)
                    {
                    if (tb.IsSystemObject == false)
                    {
                        using (StreamWriter s = new StreamWriter(@".\Database Build\4Indexes\" + tb.Name + ".sql"))
                        {
                            foreach (string tbIndex in index.Script())
                            {
                                Console.WriteLine(tbIndex);

                                s.WriteLine(tbIndex);

                            }

                            foreach (ForeignKey foreignKey in tb.ForeignKeys)
                            {
                                s.WriteLine(foreignKey.ToString());
                            }

                            
                            Console.WriteLine("-- Scripting for Table " + tb.Name);
                            //Console.WriteLine(tb.Indexes);
                            //Console.WriteLine(tb.ForeignKeys);

                            // CreateSqlFiles(scrp, tb);
                        }
                    }
                }
                    
                } 
            
        }



        private static void CreateSqlFiles(Scripter scrp, Table tb)
        {
            Directory.CreateDirectory(@".\Database Build\3SQLScripts\");
            using (StreamWriter sw = new StreamWriter(@".\Database Build\3SQLScripts\" + tb.Name + ".sql"))
            {
                try
                {
                    foreach (string st in scrp.EnumScript(new Urn[] { tb.Urn }))
                    {
                        sw.WriteLine(st);
                        Console.WriteLine(st);
                    }
                    sw.Close();
                    Console.WriteLine("--");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        private static void CreateSqlFiles(Scripter scrp, UserDefinedFunction fn)
        {
            using (StreamWriter sw = new StreamWriter(@"C:\users\block\desktop\SQLScripts\" + fn.Name + ".sql"))
            {
                try
                {
                    foreach (string st in scrp.EnumScript(new Urn[] { fn.Urn }))
                    {
                        sw.WriteLine(st);
                        Console.WriteLine(st);
                    }
                    sw.Close();
                    Console.WriteLine("--");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        /// <summary>
        /// Scripts out a specified table including data, indexes and permissions to a SQL file.
        /// </summary>
        /// <param name="server">Name of the server the stored procedure lives on</param>
        /// <param name="db">Name of the database the stored procedure lives on</param>
        /// <param name="tableName">Name of table that is to be scripted out</param>
        /// <param name="schema">Name of schema that owns table to be scripted</param>
        private static void ScriptATable(Server server, Database db, String tableName, String schema)
        {
            Table table = db.Tables[tableName, schema];

            Scripter scripter = new Scripter(server);
            scripter.Options.ScriptData = true;
            scripter.Options.Indexes = false;
            scripter.Options.Permissions = true;
            scripter.Options.SchemaQualify = true;
            scripter.Options.NonClusteredIndexes = false;

            var script = "";
            CreateSqlFiles(scripter, table);
            //try
            //{
            //    foreach (string s in scripter.EnumScript(new Urn[] { table.Urn }))
            //    {
            //        script += " " +s;
            //    }
            //}
            //catch (Exception ex)
            //{

            //    throw;
            //}
            //using (StreamWriter writer = new StreamWriter(schema + "." + tableName + ".sql"))
            //{
            //    writer.Write(script);
            //}
            //Console.WriteLine(script);
        }
        
    }
}
